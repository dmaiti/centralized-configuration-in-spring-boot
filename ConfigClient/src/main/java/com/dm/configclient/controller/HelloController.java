package com.dm.configclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HelloController {

    @Value("${database.server.host}")
    String databaseHost;

    @Value("${app.name}")
    String appName;

    @Value("${database.name}")
    String dbName;


    @GetMapping(value="/dbDetails")
    public Map getDetails(){

        Map<String,String> dbMap = new HashMap<>();
        dbMap.put("Host",databaseHost );
        dbMap.put("app",appName );
        dbMap.put("db",dbName );

        return dbMap;
    }


}
